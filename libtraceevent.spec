Name:	 	libtraceevent
Version: 	1.2.1
Release: 	1
License: 	LGPLv2+ and GPLv2+
Summary: 	Library to parse raw trace event formats

URL: 	       	https://git.kernel.org/pub/scm/libs/libtrace/libtraceevent.git/
Source0:       	https://git.kernel.org/pub/scm/libs/libtrace/libtraceevent.git/snapshot/libtraceevent-%{version}.tar.gz
BuildRequires: 	gcc
BuildRequires: 	xmlto
BuildRequires: 	asciidoc

%global __provides_exclude_from ^%{_libdir}/traceevent/plugins

%description
libtraceevent is a library to parse raw trace event formats.

%package devel
Summary: Development headers of %{name}
Requires: %{name}%{_isa} = %{version}-%{release}

%description devel
Development headers of %{name}-libs

%prep
%setup -q

%build
MANPAGE_DOCBOOK_XSL=`rpm -ql docbook-style-xsl | grep manpages/docbook.xsl`
%set_build_flags
%make_build prefix=%{_prefix} libdir=%{_libdir} MANPAGE_XSL=%{MANPAGE_DOCBOOK_XSL} all doc

%install
%make_install prefix=%{_prefix} libdir=%{_libdir} install doc-install
rm -rf %{buildroot}/%{_libdir}/libtraceevent.a

%files
%license LICENSES/LGPL-2.1
%license LICENSES/GPL-2.0
%{_libdir}/traceevent/
%{_libdir}/libtraceevent.so.%{version}
%{_libdir}/libtraceevent.so.1
%{_mandir}/man3/tep_*.3.*
%{_mandir}/man3/libtraceevent.3.*
%{_mandir}/man3/trace_seq*.3.*
%{_docdir}/%{name}-doc

%files devel
%{_includedir}/traceevent/
%{_libdir}/libtraceevent.so
%{_libdir}/pkgconfig/libtraceevent.pc

%changelog
* Fri Jan 14 2022 houyingchao <houyingchao@huawei.com> - 1.2.1-1
- Init package

